<?php

/*
 * Copyleft 2021. limitland development
 * Permission is granted to distribute this document under the terms of the
 * Creative Commons Attribution-Share-Alike License: https://creativecommons.org/licenses/by-sa/4.0/
 */

namespace ArrayTest;

require_once 'DataArray.php';

$iterations = intval($argv[1]) ?? 0;

$data = [];
for ($i = 0; $i < $iterations; $i++) {
    $data[] = 'sample';
}

$start = microtime(true);
$memstart = memory_get_usage();

$result = array_reduce($data, function (array $carry, $item) {
    $carry[] = $item;

    return $carry;
}, []);

$end = microtime(true);
$memend = memory_get_usage();

echo sprintf("%s: %01.3f s (%s results, %s memory)\n", $iterations, ($end - $start), count($result), ($memend - $memstart));
