<?php

/*
 * Copyleft 2021. limitland development
 * Permission is granted to distribute this document under the terms of the
 * Creative Commons Attribution-Share-Alike License: https://creativecommons.org/licenses/by-sa/4.0/
 */

namespace ArrayTest;

require_once 'AccumulationInterface.php';

class DataArray implements AccumulationInterface
{
    /**
     * @return array
     */
    public function addSample($existing): array
    {
        $existing[] = 'sample';

        return $existing;
    }
}
