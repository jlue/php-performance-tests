<?php

/*
 * Copyleft 2021. limitland development
 * Permission is granted to distribute this document under the terms of the
 * Creative Commons Attribution-Share-Alike License: https://creativecommons.org/licenses/by-sa/4.0/
 */

namespace ArrayTest;

use ArrayObject;

require_once 'AccumulationInterface.php';

class DataArrayObject implements AccumulationInterface
{
    /**
     * @return void
     */
    public function addSample($existing): void
    {
        $existing[] = 'sample';
    }
}
