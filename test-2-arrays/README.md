# PHP Performance Tests

## PHP Performance Tests #2 - ArrayObjects

Run the tests: 

```
for i in 1000 2000 4000 8000 16000 32000 64000 128000 256000 ; do php index-Array.php $i ; done
for i in 1000 2000 4000 8000 16000 32000 64000 128000 256000 ; do php index-ArrayObject.php $i ; done
for i in 1000 2000 4000 8000 16000 32000 64000 128000 256000 ; do php index-ReduceArray.php $i ; done
for i in 1000 2000 4000 8000 16000 32000 64000 128000 256000 ; do php index-ReduceArrayObject.php $i ; done
```
