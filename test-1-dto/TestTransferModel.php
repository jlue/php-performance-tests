<?php

/*
 * Copyleft 2021 limitland development
 * Permission is granted to distribute this document under the terms of the
 * Creative Commons Attribution-Share-Alike License: https://creativecommons.org/licenses/by-sa/4.0/
 *
 */

namespace DtoTest;

class TestTransferModel
{
    /**
     * @var string|null
     */
    public $one;

    /**
     * @var string|null
     */
    public $two;

    /**
     * @var string|null
     */
    public $three;

    /**
     * @var string|null
     */
    public $four;

    /**
     * @var string|null
     */
    public $five;

    /**
     * @var string|null
     */
    public $six;

    /**
     * @var string|null
     */
    public $seven;

    /**
     * @var string|null
     */
    public $eight;

    /**
     * @var string|null
     */
    public $nine;

    /**
     * @var string|null
     */
    public $ten;
}
