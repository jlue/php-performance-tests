<?php

/*
 * Copyleft 2021 limitland development
 * Permission is granted to distribute this document under the terms of the
 * Creative Commons Attribution-Share-Alike License: https://creativecommons.org/licenses/by-sa/4.0/
 */

namespace DtoTest;

use ArrayObject;

require_once 'ExportInterface.php';

class AArrayObject implements ExportInterface
{
    protected ArrayObject $data;

    public function __construct()
    {
        $this->data = new ArrayObject([
            'one' => 'sample',
            'two' => 'sample',
            'three' => 'sample',
            'four' => 'sample',
            'five' => 'sample',
            'six' => 'sample',
            'deven' => 'sample',
            'eight' => 'sample',
            'nine' => 'sample',
            'ten' => 'sample',
        ]);
    }

    public function export()
    {
        return $this->data;
    }
}
