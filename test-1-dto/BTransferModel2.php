<?php

/*
 * Copyleft 2021 limitland development
 * Permission is granted to distribute this document under the terms of the
 * Creative Commons Attribution-Share-Alike License: https://creativecommons.org/licenses/by-sa/4.0/
 */

namespace DtoTest;

require_once 'ImportInterface.php';
require_once 'TestTransferModel.php';

class BTransferModel2 implements ImportInterface
{
    protected TestTransferModel $data;

    public function __construct()
    {
        $this->data = new TestTransferModel();
    }

    /**
     * @param string $data
     */
    public function import($data): void
    {
        $obj = json_decode($data);

        $this->data->one = $obj->one ?? null;
        $this->data->two = $obj->two ?? null;
        $this->data->three = $obj->three ?? null;
        $this->data->four = $obj->four ?? null;
        $this->data->five = $obj->five ?? null;
        $this->data->six = $obj->six ?? null;
        $this->data->seven = $obj->seven ?? null;
        $this->data->eight = $obj->eight ?? null;
        $this->data->nine = $obj->nine ?? null;
        $this->data->ten = $obj->ten ?? null;
    }
}
