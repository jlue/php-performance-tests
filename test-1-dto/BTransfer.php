<?php

/*
 * Copyleft 2021 limitland development
 * Permission is granted to distribute this document under the terms of the
 * Creative Commons Attribution-Share-Alike License: https://creativecommons.org/licenses/by-sa/4.0/
 */

namespace DtoTest;

require_once 'ImportInterface.php';
require_once 'TestTransfer.php';

class BTransfer implements ImportInterface
{
    protected TestTransfer $data;

    public function __construct()
    {
        $this->data = new TestTransfer();
    }

    /**
     * @param TestTransfer $data
     */
    public function import($data): void
    {
        $this->data
            ->setOne($data->getOne())
            ->setTwo($data->getTwo())
            ->setThree($data->getThree())
            ->setFour($data->getFour())
            ->setFive($data->getFive())
            ->setSix($data->getSix())
            ->setSeven($data->getSeven())
            ->setEight($data->getEight())
            ->setNine($data->getNine())
            ->setTen($data->getTen());
    }
}
