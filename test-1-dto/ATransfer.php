<?php

/*
 * Copyleft 2021 limitland development
 * Permission is granted to distribute this document under the terms of the
 * Creative Commons Attribution-Share-Alike License: https://creativecommons.org/licenses/by-sa/4.0/
 */

namespace DtoTest;

require_once 'ExportInterface.php';
require_once 'TestTransfer.php';

class ATransfer implements ExportInterface
{
    protected TestTransfer $data;

    public function __construct()
    {
        $this->data = new TestTransfer();
        $this->data
            ->setOne('sample')
            ->setTwo('sample')
            ->setThree('sample')
            ->setFour('sample')
            ->setFive('sample')
            ->setSix('sample')
            ->setSeven('sample')
            ->setEight('sample')
            ->setNine('sample')
            ->setTen('sample');
    }

    public function export()
    {
        return $this->data;
    }
}
