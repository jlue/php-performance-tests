<?php

/*
 * Copyleft 2021. limitland development
 * Permission is granted to distribute this document under the terms of the
 * Creative Commons Attribution-Share-Alike License: https://creativecommons.org/licenses/by-sa/4.0/
 */

namespace DtoTest;

$iterations = intval($argv[1] ?? 0);
$start = microtime(true);

require_once 'ATransfer3.php';
require_once 'BTransfer3.php';

for ($i = 0; $i < $iterations; $i++) {
    $a = new ATransfer3();
    $b = new BTransfer3();

    $b->import($a->export());
}

$end = microtime(true);

echo sprintf("%s: %01.2f s\n", $iterations, ($end - $start));
