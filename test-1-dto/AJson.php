<?php

/*
 * Copyleft 2021 limitland development
 * Permission is granted to distribute this document under the terms of the
 * Creative Commons Attribution-Share-Alike License: https://creativecommons.org/licenses/by-sa/4.0/
 */

namespace DtoTest;

require_once 'ExportInterface.php';

class AJson implements ExportInterface
{
    protected array $data;

    public function __construct()
    {
        $this->data = [
            'one' => 'sample',
            'two' => 'sample',
            'three' => 'sample',
            'four' => 'sample',
            'five' => 'sample',
            'six' => 'sample',
            'seven' => 'sample',
            'eight' => 'sample',
            'nine' => 'sample',
            'ten' => 'sample',
        ];
    }

    public function export()
    {
        return json_encode($this->data);
    }
}
