<?php

/*
 * Copyleft 2021 limitland development
 * Permission is granted to distribute this document under the terms of the
 * Creative Commons Attribution-Share-Alike License: https://creativecommons.org/licenses/by-sa/4.0/
 *
 */

namespace DtoTest;

class TestTransfer
{
    /**
     * @var array
     */
    protected $modifiedProperties = [];

    public const ONE = 'one';

    public const TWO = 'two';

    public const THREE = 'three';

    public const FOUR = 'four';

    public const FIVE = 'five';

    public const SIX = 'six';

    public const SEVEN = 'seven';

    public const EIGHT = 'eight';

    public const NINE = 'nine';

    public const TEN = 'ten';

    /**
     * @var string|null
     */
    protected $one;

    /**
     * @var string|null
     */
    protected $two;

    /**
     * @var string|null
     */
    protected $three;

    /**
     * @var string|null
     */
    protected $four;

    /**
     * @var string|null
     */
    protected $five;

    /**
     * @var string|null
     */
    protected $six;

    /**
     * @var string|null
     */
    protected $seven;

    /**
     * @var string|null
     */
    protected $eight;

    /**
     * @var string|null
     */
    protected $nine;

    /**
     * @var string|null
     */
    protected $ten;

    /**
     * @var array
     */
    protected $transferPropertyNameMap = [
        'one' => 'one',
        'One' => 'one',
        'two' => 'two',
        'Two' => 'two',
        'three' => 'three',
        'Three' => 'three',
        'four' => 'four',
        'Four' => 'four',
        'five' => 'five',
        'Five' => 'five',
        'six' => 'six',
        'Six' => 'six',
        'seven' => 'seven',
        'Seven' => 'seven',
        'eight' => 'eight',
        'Eight' => 'eight',
        'nine' => 'nine',
        'Nine' => 'nine',
        'ten' => 'ten',
        'Ten' => 'ten',
    ];

    /**
     * @var array
     */
    protected $transferMetadata = [
        self::ONE => [
            'type' => 'string',
            'name_underscore' => 'one',
            'is_collection' => false,
            'is_transfer' => false,
            'is_value_object' => false,
            'rest_request_parameter' => 'no',
            'is_associative' => false,
            'is_nullable' => false,
        ],
        self::TWO => [
            'type' => 'string',
            'name_underscore' => 'two',
            'is_collection' => false,
            'is_transfer' => false,
            'is_value_object' => false,
            'rest_request_parameter' => 'no',
            'is_associative' => false,
            'is_nullable' => false,
        ],
        self::THREE => [
            'type' => 'string',
            'name_underscore' => 'three',
            'is_collection' => false,
            'is_transfer' => false,
            'is_value_object' => false,
            'rest_request_parameter' => 'no',
            'is_associative' => false,
            'is_nullable' => false,
        ],
        self::FOUR => [
            'type' => 'string',
            'name_underscore' => 'four',
            'is_collection' => false,
            'is_transfer' => false,
            'is_value_object' => false,
            'rest_request_parameter' => 'no',
            'is_associative' => false,
            'is_nullable' => false,
        ],
        self::FIVE => [
            'type' => 'string',
            'name_underscore' => 'five',
            'is_collection' => false,
            'is_transfer' => false,
            'is_value_object' => false,
            'rest_request_parameter' => 'no',
            'is_associative' => false,
            'is_nullable' => false,
        ],
        self::SIX => [
            'type' => 'string',
            'name_underscore' => 'six',
            'is_collection' => false,
            'is_transfer' => false,
            'is_value_object' => false,
            'rest_request_parameter' => 'no',
            'is_associative' => false,
            'is_nullable' => false,
        ],
        self::SEVEN => [
            'type' => 'string',
            'name_underscore' => 'seven',
            'is_collection' => false,
            'is_transfer' => false,
            'is_value_object' => false,
            'rest_request_parameter' => 'no',
            'is_associative' => false,
            'is_nullable' => false,
        ],
        self::EIGHT => [
            'type' => 'string',
            'name_underscore' => 'eight',
            'is_collection' => false,
            'is_transfer' => false,
            'is_value_object' => false,
            'rest_request_parameter' => 'no',
            'is_associative' => false,
            'is_nullable' => false,
        ],
        self::NINE => [
            'type' => 'string',
            'name_underscore' => 'nine',
            'is_collection' => false,
            'is_transfer' => false,
            'is_value_object' => false,
            'rest_request_parameter' => 'no',
            'is_associative' => false,
            'is_nullable' => false,
        ],
        self::TEN => [
            'type' => 'string',
            'name_underscore' => 'ten',
            'is_collection' => false,
            'is_transfer' => false,
            'is_value_object' => false,
            'rest_request_parameter' => 'no',
            'is_associative' => false,
            'is_nullable' => false,
        ],
    ];

    /**
     * @param string|null $one
     *
     * @return $this
     */
    public function setOne($one)
    {
        $this->one = $one;
        $this->modifiedProperties[self::ONE] = true;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getOne()
    {
        return $this->one;
    }

    /**
     * @param string|null $two
     *
     * @return $this
     */
    public function setTwo($two)
    {
        $this->two = $two;
        $this->modifiedProperties[self::TWO] = true;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getTwo()
    {
        return $this->two;
    }

    /**
     * @param string|null $three
     *
     * @return $this
     */
    public function setThree($three)
    {
        $this->three = $three;
        $this->modifiedProperties[self::THREE] = true;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getThree()
    {
        return $this->three;
    }

    /**
     * @param string|null $four
     *
     * @return $this
     */
    public function setFour($four)
    {
        $this->four = $four;
        $this->modifiedProperties[self::FOUR] = true;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getFour()
    {
        return $this->four;
    }

    /**
     * @param string|null $five
     *
     * @return $this
     */
    public function setFive($five)
    {
        $this->five = $five;
        $this->modifiedProperties[self::FIVE] = true;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getFive()
    {
        return $this->five;
    }

    /**
     * @param string|null $six
     *
     * @return $this
     */
    public function setSix($six)
    {
        $this->six = $six;
        $this->modifiedProperties[self::SIX] = true;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSix()
    {
        return $this->six;
    }

    /**
     * @param string|null $seven
     *
     * @return $this
     */
    public function setSeven($seven)
    {
        $this->seven = $seven;
        $this->modifiedProperties[self::SEVEN] = true;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSeven()
    {
        return $this->seven;
    }

    /**
     * @param string|null $eight
     *
     * @return $this
     */
    public function setEight($eight)
    {
        $this->eight = $eight;
        $this->modifiedProperties[self::EIGHT] = true;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getEight()
    {
        return $this->eight;
    }

    /**
     * @param string|null $nine
     *
     * @return $this
     */
    public function setNine($nine)
    {
        $this->nine = $nine;
        $this->modifiedProperties[self::NINE] = true;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getNine()
    {
        return $this->nine;
    }

    /**
     * @param string|null $ten
     *
     * @return $this
     */
    public function setTen($ten)
    {
        $this->ten = $ten;
        $this->modifiedProperties[self::TEN] = true;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getTen()
    {
        return $this->ten;
    }

    /**
     * @param array $data
     * @param bool $ignoreMissingProperty
     * @return TestTransfer
     */
    public function fromArray(array $data, $ignoreMissingProperty = false)
    {
        foreach ($data as $property => $value) {
            $normalizedPropertyName = $this->transferPropertyNameMap[$property] ?? null;

            switch ($normalizedPropertyName) {
                case 'one':
                case 'two':
                case 'three':
                case 'four':
                case 'five':
                case 'six':
                case 'seven':
                case 'eight':
                case 'nine':
                case 'ten':
                    $this->$normalizedPropertyName = $value;
                    $this->modifiedProperties[$normalizedPropertyName] = true;
                    break;
                default:
                    if (!$ignoreMissingProperty) {
                        throw new \InvalidArgumentException(sprintf('Missing property `%s` in `%s`', $property, static::class));
                    }
            }
        }

        return $this;
    }

    /**
    * @param bool $camelCasedKeys
    * @return array
    */
    public function toArray($camelCasedKeys = false)
    {
        if (!$camelCasedKeys) {
            return $this->toArrayNotRecursiveNotCamelCased();
        }
        if ($camelCasedKeys) {
            return $this->toArrayNotRecursiveCamelCased();
        }
    }

    /**
    * @return array
    */
    public function toArrayNotRecursiveCamelCased()
    {
        return [
            'one' => $this->one,
            'two' => $this->two,
            'three' => $this->three,
            'four' => $this->four,
            'five' => $this->five,
            'six' => $this->six,
            'seven' => $this->seven,
            'eight' => $this->eight,
            'nine' => $this->nine,
            'ten' => $this->ten,
        ];
    }

    /**
    * @return array
    */
    public function toArrayNotRecursiveNotCamelCased()
    {
        return [
            'one' => $this->one,
            'two' => $this->two,
            'three' => $this->three,
            'four' => $this->four,
            'five' => $this->five,
            'six' => $this->six,
            'seven' => $this->seven,
            'eight' => $this->eight,
            'nine' => $this->nine,
            'ten' => $this->ten,
        ];
    }
}
