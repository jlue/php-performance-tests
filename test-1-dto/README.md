# PHP Performance Tests

## Part #1 PHP Performance Tests #1 - DTOs

Run the tests: 

```
for i in 10000 100000 1000000 5000000 10000000 ; do php index-Transfer.php $i ; done
for i in 10000 100000 1000000 5000000 10000000 ; do php index-Array.php $i ; done
for i in 10000 100000 1000000 5000000 10000000 ; do php index-Json.php $i ; done
for i in 10000 100000 1000000 5000000 10000000 ; do php index-ArrayObject.php $i ; done
for i in 10000 100000 1000000 5000000 10000000 ; do php index-Transfer2.php $i ; done
for i in 10000 100000 1000000 5000000 10000000 ; do php index-Transfer3.php $i ; done
for i in 10000 100000 1000000 5000000 10000000 ; do php index-Model1.php $i ; done
for i in 10000 100000 1000000 5000000 10000000 ; do php index-Model2.php $i ; done
```
