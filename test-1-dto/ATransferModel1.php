<?php

/*
 * Copyleft 2021 limitland development
 * Permission is granted to distribute this document under the terms of the
 * Creative Commons Attribution-Share-Alike License: https://creativecommons.org/licenses/by-sa/4.0/
 */

namespace DtoTest;

require_once 'ExportInterface.php';
require_once 'TestTransferModel.php';

class ATransferModel1 implements ExportInterface
{
    protected TestTransferModel $data;

    public function __construct()
    {
        $this->data = new TestTransferModel();

        $this->data->one = 'sample';
        $this->data->two = 'sample';
        $this->data->three = 'sample';
        $this->data->four = 'sample';
        $this->data->five = 'sample';
        $this->data->six = 'sample';
        $this->data->seven = 'sample';
        $this->data->eight = 'sample';
        $this->data->nine = 'sample';
        $this->data->ten = 'sample';
    }

    public function export()
    {
        return $this->data;
    }
}
