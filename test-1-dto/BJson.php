<?php

/*
 * Copyleft 2021 limitland development
 * Permission is granted to distribute this document under the terms of the
 * Creative Commons Attribution-Share-Alike License: https://creativecommons.org/licenses/by-sa/4.0/
 */

namespace DtoTest;

require_once 'ImportInterface.php';

class BJson implements ImportInterface
{
    protected array $data;

    public function __construct()
    {
        $this->data = [];
    }

    public function import($data): void
    {
        $this->data = json_decode($data, true);
    }
}
