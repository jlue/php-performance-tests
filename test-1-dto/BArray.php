<?php

/*
 * Copyleft 2021 limitland development
 * Permission is granted to distribute this document under the terms of the
 * Creative Commons Attribution-Share-Alike License: https://creativecommons.org/licenses/by-sa/4.0/
 */

namespace DtoTest;

require_once 'ImportInterface.php';

class BArray implements ImportInterface
{
    protected array $data;

    public function __construct()
    {
        $this->data = [];
    }

    public function import($data): void
    {
        $this->data['one'] = $data['one'];
        $this->data['two'] = $data['two'];
        $this->data['three'] = $data['three'];
        $this->data['four'] = $data['four'];
        $this->data['five'] = $data['five'];
        $this->data['six'] = $data['six'];
        $this->data['seven'] = $data['seven'];
        $this->data['eight'] = $data['eight'];
        $this->data['nine'] = $data['nine'];
        $this->data['ten'] = $data['ten'];
    }
}
