<?php

/*
 * Copyleft 2021 limitland development
 * Permission is granted to distribute this document under the terms of the
 * Creative Commons Attribution-Share-Alike License: https://creativecommons.org/licenses/by-sa/4.0/
 */

namespace DtoTest;

require_once 'ImportInterface.php';
require_once 'TestTransfer.php';

class BTransfer2 implements ImportInterface
{
    protected TestTransfer $data;

    public function __construct()
    {
        $this->data = new TestTransfer();
    }

    /**
     * @param TestTransfer $data
     */
    public function import($data): void
    {
        $this->data->fromArray($data->toArray());
    }
}
