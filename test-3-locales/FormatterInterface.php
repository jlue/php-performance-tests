<?php

/*
 * Copyleft 2021 limitland development
 * Permission is granted to distribute this document under the terms of the
 * Creative Commons Attribution-Share-Alike License: https://creativecommons.org/licenses/by-sa/4.0/
 */

namespace LocalesTest;

interface FormatterInterface
{
    /**
     * @param mixed $sample
     *
     * @return mixed
     */
    public function formatSample($sample): string ;
}
