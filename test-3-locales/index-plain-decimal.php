<?php

/*
 * Copyleft 2021. limitland development
 * Permission is granted to distribute this document under the terms of the
 * Creative Commons Attribution-Share-Alike License: https://creativecommons.org/licenses/by-sa/4.0/
 */

namespace LocalesTest;

require_once 'DataLocalizedDecimal.php';

$iterations = intval($argv[1] ?? 0);

$sample = "20210901.1050";

$uut = new DataLocalizedDecimal('C');

setlocale(LC_ALL, 'C');

$sampleResult = $uut->plainSample($sample);
echo sprintf("%s (%s): ", $iterations, $sampleResult);

$start = microtime(true);

for ($i = 0; $i < $iterations; $i++) {
    $uut->plainSample($sample);
}

$end = microtime(true);

echo sprintf("%01.3f s\n", ($end - $start));
