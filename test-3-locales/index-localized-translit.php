<?php

/*
 * Copyleft 2021. limitland development
 * Permission is granted to distribute this document under the terms of the
 * Creative Commons Attribution-Share-Alike License: https://creativecommons.org/licenses/by-sa/4.0/
 */

namespace LocalesTest;

require_once 'DataLocalizedTranslit.php';

$iterations = intval($argv[1] ?? 0);

setlocale(LC_ALL, 'C');
$sample = 'äöü ÄÖÜ ß €';
$sampleLocaleName ='de_DE.UTF8';

$uut = new DataLocalizedTranslit();

$sampleResult = $uut->localizedSample($sample, $sampleLocaleName);
echo sprintf("%s (%s): ", $iterations, $sampleResult);

$start = microtime(true);

for ($i = 0; $i < $iterations; $i++) {
    $uut->localizedSample($sample, $sampleLocaleName);
}

$end = microtime(true);

echo sprintf("%01.3f s\n", ($end - $start));
