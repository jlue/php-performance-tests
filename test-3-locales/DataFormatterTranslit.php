<?php

/*
 * Copyleft 2021. limitland development
 * Permission is granted to distribute this document under the terms of the
 * Creative Commons Attribution-Share-Alike License: https://creativecommons.org/licenses/by-sa/4.0/
 */

namespace LocalesTest;

use Transliterator;

require_once 'FormatterInterface.php';

class DataFormatterTranslit implements FormatterInterface
{
    /**
     * @var Transliterator
     */
    protected Transliterator $translitarator;

    /**
     * @param string $transliteratorIds
     */
    public function __construct(string $transliteratorIds)
    {
        $this->translitarator = Transliterator::create($transliteratorIds);
    }

    /**
     * @param mixed $sample
     *
     * @return string
     */
    public function formatSample($sample): string
    {
        return $this->translitarator->transliterate($sample);
    }
}
