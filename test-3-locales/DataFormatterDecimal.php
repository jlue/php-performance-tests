<?php

/*
 * Copyleft 2021. limitland development
 * Permission is granted to distribute this document under the terms of the
 * Creative Commons Attribution-Share-Alike License: https://creativecommons.org/licenses/by-sa/4.0/
 */

namespace LocalesTest;

use NumberFormatter;

require_once 'FormatterInterface.php';

class DataFormatterDecimal implements FormatterInterface
{
    /**
     * @var NumberFormatter
     */
    protected NumberFormatter $formatter;

    /**
     * @param string $localeName
     */
    public function __construct(string $localeName)
    {
        $this->formatter = new NumberFormatter($localeName, NumberFormatter::DECIMAL);
    }

    /**
     * @param mixed $sample
     *
     * @return string
     */
    public function formatSample($sample): string
    {
        return $this->formatter->format($sample);
    }
}
