# PHP Performance Tests

## PHP Performance Tests #3 - Locales

Run the tests: 

```
for TEST in \
  index-plain-translit.php \
  index-plain-datetime.php \
  index-plain-decimal.php \
  index-plain-currency.php \
  index-localized-translit.php \
  index-localized-datetime.php \
  index-localized-decimal.php \
  index-localized-currency.php \
  index-plain-formatter-translit.php \
  index-plain-formatter-datetime.php \
  index-plain-formatter-decimal.php \
  index-plain-formatter-currency.php \
  index-localized-formatter-translit.php \
  index-localized-formatter-datetime.php \
  index-localized-formatter-decimal.php \
  index-localized-formatter-currency.php \
  ; do
  
  echo "${TEST}" 
  for i in 1000 2000 4000 8000 16000 32000 64000 128000 256000 \
      512000 1024000 2048000 4096000 8192000 16348000 32768000 65536000; do php "${TEST}" "${i}" ; done

done
```
