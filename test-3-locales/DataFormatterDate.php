<?php

/*
 * Copyleft 2021. limitland development
 * Permission is granted to distribute this document under the terms of the
 * Creative Commons Attribution-Share-Alike License: https://creativecommons.org/licenses/by-sa/4.0/
 */

namespace LocalesTest;

use IntlDateFormatter;
use NumberFormatter;

require_once 'FormatterInterface.php';

class DataFormatterDate implements FormatterInterface
{
    /**
     * @var IntlDateFormatter
     */
    protected IntlDateFormatter $formatter;

    /**
     * @param string $localeName
     */
    public function __construct(string $localeName)
    {
        $this->formatter = new IntlDateFormatter($localeName, IntlDateFormatter::LONG, IntlDateFormatter::SHORT);
    }

    /**
     * @param mixed $sample
     *
     * @return string
     */
    public function formatSample($sample): string
    {
        return $this->formatter->format($sample);
    }
}
