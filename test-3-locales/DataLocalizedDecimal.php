<?php

/*
 * Copyleft 2021. limitland development
 * Permission is granted to distribute this document under the terms of the
 * Creative Commons Attribution-Share-Alike License: https://creativecommons.org/licenses/by-sa/4.0/
 */

namespace LocalesTest;

require_once 'LocalizationInterface.php';

class DataLocalizedDecimal implements LocalizationInterface
{
    protected const LOCALE_FIELD = LC_NUMERIC;

    public function __construct(string $localeName)
    {
        setlocale(self::LOCALE_FIELD, $localeName);

        $localevconv = localeconv();

        $this->decimals = 2;
        $this->decimal_separator = $localevconv['decimal_point'];
        $this->thousands_separator = $localevconv['thousands_sep'];
    }


    /**
     * @param mixed $sample
     *
     * @return string
     */
    public function plainSample($sample): string
    {
        return number_format($sample, $this->decimals, $this->decimal_separator, $this->thousands_separator);
    }

    /**
     * @param mixed $sample
     * @param string $localeName
     *
     * @return string
     */
    public function localizedSample($sample, string $localeName): string
    {
        $currentLocale = setlocale(self::LOCALE_FIELD, 0);

        setlocale(self::LOCALE_FIELD, $localeName);

        $result = number_format($sample, $this->decimals, $this->decimal_separator, $this->thousands_separator);

        setlocale(self::LOCALE_FIELD, $currentLocale);

        return $result;
    }
}
