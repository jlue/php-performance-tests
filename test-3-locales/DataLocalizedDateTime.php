<?php

/*
 * Copyleft 2021. limitland development
 * Permission is granted to distribute this document under the terms of the
 * Creative Commons Attribution-Share-Alike License: https://creativecommons.org/licenses/by-sa/4.0/
 */

namespace LocalesTest;

require_once 'LocalizationInterface.php';

class DataLocalizedDateTime implements LocalizationInterface
{
    protected const LOCALE_FIELD = LC_TIME;

    /**
     * @param mixed $sample
     *
     * @return string
     */
    public function plainSample($sample): string
    {
        return strftime('%A %x', $sample);
    }

    /**
     * @param mixed $sample
     * @param string $localeName
     *
     * @return string
     */
    public function localizedSample($sample, string $localeName): string
    {
        $currentLocale = setlocale(self::LOCALE_FIELD, 0);

        setlocale(self::LOCALE_FIELD, $localeName);

        $result = strftime('%A %x', $sample);

        setlocale(self::LOCALE_FIELD, $currentLocale);

        return $result;
    }
}
