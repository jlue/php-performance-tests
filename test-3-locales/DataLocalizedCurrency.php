<?php

/*
 * Copyleft 2021. limitland development
 * Permission is granted to distribute this document under the terms of the
 * Creative Commons Attribution-Share-Alike License: https://creativecommons.org/licenses/by-sa/4.0/
 */

namespace LocalesTest;

require_once 'LocalizationInterface.php';

class DataLocalizedCurrency implements LocalizationInterface
{
    protected const LOCALE_FIELD = LC_MONETARY;

    /**
     * @param mixed $sample
     *
     * @return string
     */
    public function plainSample($sample): string
    {
        return money_format('%i', $sample);
    }

    /**
     * @param mixed $sample
     * @param string $localeName
     *
     * @return string
     */
    public function localizedSample($sample, string $localeName): string
    {
        $currentLocale = setlocale(self::LOCALE_FIELD, 0);

        setlocale(self::LOCALE_FIELD, $localeName);

        $result = money_format('%i', $sample);

        setlocale(self::LOCALE_FIELD, $currentLocale);

        return $result;
    }
}
