<?php

/*
 * Copyleft 2021. limitland development
 * Permission is granted to distribute this document under the terms of the
 * Creative Commons Attribution-Share-Alike License: https://creativecommons.org/licenses/by-sa/4.0/
 */

$iterations = $argv[1] ?? 0;
$sample = $argv[2] ?? 'äöü ÄÖÜ ß €';
$ruleset = $argv[3] ?? 'de-ASCII;Any-Latin;Latin-ASCII';

echo sprintf("iterations: %s\nsample: %s\nruleset: %s\n", $iterations, $sample, $ruleset);

$translitarator = Transliterator::create($ruleset);

$sampleResult = $translitarator->transliterate($sample);
echo sprintf("result: %s\n", $sampleResult);

$start = microtime(true);

for ($i = 0; $i < $iterations; $i++) {
    $translitarator->transliterate($sample);
}

$end = microtime(true);

echo sprintf("time: %01.3f s\n", ($end - $start));
