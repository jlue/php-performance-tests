<?php

/*
 * Copyleft 2021. limitland development
 * Permission is granted to distribute this document under the terms of the
 * Creative Commons Attribution-Share-Alike License: https://creativecommons.org/licenses/by-sa/4.0/
 */

namespace LocalesTest;

require_once 'LocalizationInterface.php';

class DataLocalizedTranslit implements LocalizationInterface
{
    protected const LOCALE_FIELD = LC_CTYPE;

    /**
     * @param mixed $sample
     *
     * @return string
     */
    public function plainSample($sample): string
    {
        return iconv('UTF-8','ASCII//TRANSLIT', $sample);
    }

    /**
     * @param mixed $sample
     * @param string $localeName
     *
     * @return string
     */
    public function localizedSample($sample, string $localeName): string
    {
        $currentLocale = setlocale(self::LOCALE_FIELD, 0);

        setlocale(self::LOCALE_FIELD, $localeName);

        $result = iconv('UTF-8','ASCII//TRANSLIT', $sample);

        setlocale(self::LOCALE_FIELD, $currentLocale);

        return $result;
    }
}
