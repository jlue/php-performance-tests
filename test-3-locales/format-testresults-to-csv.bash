#!/bin/bash

OUTFILE="testresults.csv"

echo -n ';' > "${OUTFILE}"
readarray -t ARRAY < <(grep index testresults.txt | sed 's/index-//g;s/.php//g;s/-/ /g'); IFS=';'; echo "${ARRAY[*]}" >> "${OUTFILE}"

  for i in 1000 2000 4000 8000 16000 32000 64000 128000 256000 \
      512000 1024000 2048000 4096000 8192000 16348000 32768000 65536000; do 
  
    echo -n "$i;" >> "${OUTFILE}"
    readarray -t ARRAY < <(grep "^$i " testresults.txt | cut -d')' -f2 | sed 's/[ :s]//g'); IFS=';'; echo "${ARRAY[*]}" >> "${OUTFILE}"

  done

