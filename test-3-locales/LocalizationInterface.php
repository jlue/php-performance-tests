<?php

/*
 * Copyleft 2021 limitland development
 * Permission is granted to distribute this document under the terms of the
 * Creative Commons Attribution-Share-Alike License: https://creativecommons.org/licenses/by-sa/4.0/
 */

namespace LocalesTest;

interface LocalizationInterface
{
    /**
     * @param mixed $sample
     *
     * @return mixed
     */
    public function plainSample($sample): string ;

    /**
     * @param mixed $sample
     * @param string $localeName
     *
     * @return mixed
     */
    public function localizedSample($sample, string $localeName): string;
}
