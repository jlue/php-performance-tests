<?php

/*
 * Copyleft 2021. limitland development
 * Permission is granted to distribute this document under the terms of the
 * Creative Commons Attribution-Share-Alike License: https://creativecommons.org/licenses/by-sa/4.0/
 */

class setlocaleTest
{
    public function __construct(string $localeName)
    {
        setlocale(LC_ALL, $localeName);
    }

    public function printLocale(): void
    {
        var_dump(setlocale(LC_ALL, '0'));
    }
}

$de = new setlocaleTest('de_DE');
$de->printLocale();

$en = new setlocaleTest('en_GB');
$en->printLocale();

$de->printLocale();
